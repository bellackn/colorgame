// INITIAL VARIABLES
var numSquares = 6;
var colors = [];
var pickedColor;

// SELECTORS
var colorDisplay = document.querySelector("#colorDisplay");
var h1 = document.querySelector("h1");
var messageDisplay = document.querySelector("#messageDisplay");
var modeButtons = document.querySelectorAll(".mode");
var resetButton = document.querySelector("#reset");
var squares = document.querySelectorAll(".square");

init();

function changeColors(color){
    // loop through all squares
    for(var i = 0; i < squares.length; i++){
        // change each color to match given color
        squares[i].style.backgroundColor = color;
    }
}

function generateRandomColors(num){
    // make an array
    var arr = [];
    // add num random colors to array
    for(var i = 0; i < num; i++){
        // get random color and push into arr
        arr.push(randomColor());
    }

    return arr;
}

function init(){
    // add event listener for reset button
    resetButton.addEventListener("click", function(){
        reset();
    })

    initModeButtons();

    initSquares();

    reset();
}

function initModeButtons(){
    // Add event listeners for different modes
    for(var i = 0; i < modeButtons.length; i++){
        modeButtons[i].addEventListener("click", function(){
            modeButtons[0].classList.remove("selected");
            modeButtons[1].classList.remove("selected");
            this.classList.add("selected");
            this.textContent === "Easy" ? numSquares = 3 : numSquares = 6;
            reset();
        })
    }
}

function initSquares(){
    for (var i = 0; i < squares.length; i++) {
        // add event listeners to squares
        squares[i].addEventListener("click", function(){
            // grab color of clicked square
            var clickedColor = this.style.backgroundColor;
            // compare clickedColor to pickedColor
            if (clickedColor === pickedColor){
                messageDisplay.textContent = "You've got it!"
                changeColors(clickedColor);
                h1.style.backgroundColor = clickedColor;
                resetButton.textContent = "Play again";
            } else {
                this.style.backgroundColor = "#232323";
                messageDisplay.textContent = "Try again!"
            }
        })
    }
}

function pickColor(){
    var random = Math.floor(Math.random() * colors.length);

    return colors[random];
}

function randomColor(){
    // pick a "red" from 0 - 255
    var r = Math.floor(Math.random() * 256);
    // pick a "green" from 0 - 255
    var g = Math.floor(Math.random() * 256);
    // pick a "blue" from 0 - 255
    var b = Math.floor(Math.random() * 256);

    return "rgb(" + r + ", " + g + ", " + b + ")";
}

function reset(){
    // generate all new colors
    colors = generateRandomColors(numSquares);
    // pick a new random color from array
    pickedColor = pickColor();
    // reset display
    colorDisplay.textContent = pickedColor;
    messageDisplay.textContent = "";
    resetButton.textContent = "New Colors";
    // change colors of squares
    for(var i = 0; i < squares.length; i++){
        // fill squares depending on mode selection
        if(colors[i]){
            squares[i].style.display = "block";
            squares[i].style.backgroundColor = colors[i];
        } else {
            squares[i].style.display = "none";
        }
    }
    h1.style.backgroundColor = "steelblue";
}